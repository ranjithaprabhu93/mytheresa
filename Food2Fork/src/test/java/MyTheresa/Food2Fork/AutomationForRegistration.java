/*
  Created By: Ranjita Kukundur
  Date: 17-06-2019
  Description : Automating Food2Fork search Functionality, by searching "cookie monster cupcakes" as the searchTerm, and print in console
*/
package MyTheresa.Food2Fork;

import java.util.concurrent.TimeUnit;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AutomationForRegistration {

	@Test
	public void Food2Fork() {
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.food2fork.com/default/user/register");

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// Enter all the details required for registration
		driver.findElement(By.xpath("//*[@id=\'auth_user_first_name\']")).sendKeys("Test");
		driver.findElement(By.xpath("//*[@id=\'auth_user_last_name\'] ")).sendKeys("User");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@id=\'auth_user_email\']")).sendKeys("Testuser88@gmail.com");
		driver.findElement(By.xpath("//*[@id=\'auth_user_password\']")).sendKeys("7q8w9e4a5s6d9a");
		driver.findElement(By.xpath("//input[@name='password_two']")).sendKeys("7q8w9e4a5s6d9a");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//ignoring captcha for the moment
		
		// click on Register button
		driver.findElement(By.xpath("//*[@id=\'submit_record__row\']/td[2]/input")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.close();
	}
}
