/*
  Created By: Ranjita Kukundur
  Date: 17-06-2019
  Description : Automating Food2Fork search Functionality, by searching "cookie monster cupcakes" as the searchTerm, and print in console
*/

package MyTheresa.Food2Fork;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchCookieMonsterCupcake {

	@Test
	public void IsGoogleDriverWorking() {
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.food2fork.com/");
		WebDriverWait wait = new WebDriverWait(driver, 10);
		// Get the CSS of food search input box, enter a text "cookie monster cupcakes"
		String CssInputField = "#typeahead";
		WebElement InputField = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(CssInputField)));
		InputField.sendKeys("cookie monster cupcakes");
		// Click enter to trigger the search		
		InputField.sendKeys(Keys.ENTER);
		// print on the console once successful
		System.out.println("Test success! cookie monster cupcakes found!!");
		driver.close();
	}
}
