
public class NumberDivisibility {

	public static void main(String[] arg) {
		Integer number = Integer.parseInt(arg[arg.length - 1]);
		System.out.println(CheckNumberDivisibility(number));
	}

	public static String CheckNumberDivisibility(Integer number) {
		String returnString = "";
		if (number % 3 == 0) {
			returnString += "my";
		}
		if (number % 5 == 0) {
			returnString += "theresa";
		}
		/*
		 * if (number % 7 == 0) { returnString += "clothes"; }
		 */
		if (returnString.length() > 0) {
			return returnString;
		} else {
			return Integer.toString(number);
		}
	}
}
